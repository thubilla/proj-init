const test = require('tape');
const {transformPackageData} = require('../lib/transform-data');

test('transformPackageData', function(assert){
	var fixtures = { 
		package_name: 'Project', 
		version_number: '0.0.1', 
		entry_point: 'script.js', 
		keywords: 'blah blah blah', 
	};

	{
		let message = 'transforms object into expected data structure';
		let actual = transformPackageData(fixtures);

		assert.ok(actual.hasOwnProperty('name'), message);
		assert.ok(actual.hasOwnProperty('version'), message);
		assert.ok(actual.hasOwnProperty('main'), message);
		assert.ok(actual.hasOwnProperty('keywords'), message);
		assert.ok(actual.hasOwnProperty('author'), message);
		assert.ok(actual.hasOwnProperty('license'), message);
	}

	{
		let message = 'transforms "keywords" string to array';
		let actual = Array.isArray(transformPackageData(fixtures).keywords);
		let expected = true;

		assert.equal(actual, expected, message);
	}

	assert.end();
});

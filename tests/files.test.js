const fs = require('fs');
const test = require('tape');
const { createEnvFile, createJSONFile } = require('../lib/files');

const fixtures = {
	configurable: true, 
	string: 'true', 
	number: 123
};

test('createEnvFile', function(assert){
	createEnvFile(__dirname, fixtures);

	{
		let message = 'creates .env file in pwd';
		let actual = fs.existsSync(`${__dirname}/.env`);
		let expected = true;
		assert.equal(actual, expected, message);
	}

	{
		let message = 'writes data object input to .env file in pwd';
		let actual = fs.readFileSync(`${__dirname}/.env`).toString().trim();
		let expected = 'CONFIGURABLE=true\nSTRING=true\nNUMBER=123'; 
		assert.equal(actual, expected, message);
	}

	fs.unlinkSync(`${__dirname}/.env`);
	assert.end();
});

test('createJSONFile', function(assert){
	createJSONFile(`${__dirname}/test.json`, fixtures);	

	{
		let message = 'creates JSON file in pwd';
		let actual = fs.existsSync(`${__dirname}/test.json`);
		let expected = true;
		assert.equal(actual, expected, message);
	}

	{
		let fixture = require('./test.json');

		for(let [key, value] of Object.entries(fixture)){
			let message = 'writes data input to JSON file in pwd';
			let actual = fixture[key]; 
			let expected = value;

			assert.equal(actual, expected, message);
		}
	}

	fs.unlinkSync(`${__dirname}/test.json`);
	assert.end();
});


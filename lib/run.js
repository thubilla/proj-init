/* eslint no-console: 0  */
const shell = require('shelljs');
const chalk = require('chalk');

const { createEnvFile, createGitignoreFile, createJSONFile } = require('./files');
const { askNPMInitQuestions, askAuthQuestions }  = require('./inquirer');
const { transformPackageData } = require('./transform-data');

const bitbucketWarning = `
${chalk.red('WARNING:')}

${chalk.yellow('The Bitbucket API does not currently allow remote repositories to be created from a local project!')}

This tool will initialize the given project as a local git repository to be used with an existing remote repository.
All user details have been saved to a local '.env' file in your project (dont't forget to include this in your '.gitignore').

Learn to sync up a remote repository on Bitbucket with your local project visit:
${chalk.cyan('https://confluence.atlassian.com/bitbucket/create-a-git-repository-759857290.html')}

`;
	
async function run(target_dir = process.cwd(), description = ''){
	try{
		var npmData = await askNPMInitQuestions();
		var gitData = await askAuthQuestions();	
		var pkgData = transformPackageData(Object.assign({}, npmData, { author: gitData.user_name, description }));

		await	createEnvFile(target_dir, gitData);
		await	createJSONFile(`${target_dir}/package.json`, pkgData);
		await	createGitignoreFile(target_dir);

		if(gitData.version_control_host === 'bitbucket'){
			console.log(bitbucketWarning);	
		}

		shell.cd(target_dir);
		shell.exec('git init');
		//shell.exec(`git add ${target_dir}/.gitignore`);
	} catch(err) {
		console.error(err.message);
	} finally {
		console.log(chalk.green('Done!'));	
	}
}

async function auth(dir = process.cwd()){
	var data = await askAuthQuestions();
	await createEnvFile(dir, data);
	console.log(chalk.green('Saved .env file'));
	return data;
}

exports.run = run;
exports.auth = auth;

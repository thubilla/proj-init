const fs = require('fs');
const { askGitignoreQuestions } = require('./inquirer');

exports.createEnvFile = async function(dir = process.cwd(), options = {}){
	var data = [];
	for(let [key, value] of Object.entries(options)){
		data.push(`${key.toUpperCase()}=${value}`);
	}	

	if(fs.existsSync(`${dir}/.env`)){
		fs.appendFileSync(`${dir}/.env`, data.join('\n'));
	}

	await fs.writeFileSync(`${dir}/.env`, data.join('\n'));
	return;
};

exports.createJSONFile = async function(fileName, options = {}){
	fileName = String(fileName);
	if(fs.existsSync(fileName)){
		throw new Error(`${fileName} already exists`);

	} 	
	await fs.writeFileSync(fileName, JSON.stringify(options));
	return;
};

exports.createGitignoreFile = async function(dir){
	var fileList = fs.readdirSync(dir).filter(file => file !== '.git' || file !== '.gitignore');	
	if(!fileList.length){
		return;
	} 

	var answers = await askGitignoreQuestions(fileList);	
	if(!answers.ignore.length){
		return;
	}

	await	fs.writeFileSync('.gitignore', answers.ignore.join('\n'));
	return;
};

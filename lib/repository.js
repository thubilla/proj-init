/* eslint-disable */
const bitbucket = require('bitbucket-api');
const github = require('@octokit/rest');

async function createBitbucketRepo({ username, password, project }){	
	let client = await bitbucket.createClient({ username, password });
	return client.users().oauth(username);
}

exports.createBitbucketRepo = createBitbucketRepo;

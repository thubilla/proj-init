exports.transformPackageData = function({ package_name, version_number, description, entry_point, keywords, author, license }){
	keywords = keywords ? keywords.split(' ') : [];
	
	return { 
		name: package_name || '', 
		version: version_number || '',
		description: description || '', 
		main: entry_point || '', 
		keywords, 
		author: author || '',
		license: license || ''
	};
};

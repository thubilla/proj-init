const path = require('path');
const inquirer = require('inquirer');

exports.askNPMInitQuestions = function(){
	const base = path.basename(process.cwd());
	const questions = [{
		type: 'input',
		name: 'package_name',
		message: 'Package Name:',
		default: base.replace(/\s/g, '-')
	}, {
		type: 'input',
		name: 'version_number',
		message: 'Version Number:',
		default: '0.0.1'
	}, {
		type: 'input',
		name: 'entry_point',
		message: 'Entry Point:',
		default: 'index.js',
	}, {
		type: 'input',
		name: 'keywords',
		message: 'Keywords:',
	}, {
		type: 'input',
		name: 'license',
		message: 'License:',
		default: 'ISC'
	}];

	return inquirer.prompt(questions);
};

exports.askAuthQuestions = function(){
	var questions = [{
		type: 'list',
		name: 'version_control_host',
		message: 'Version Control Host:',
		choices: ['github', 'bitbucket'],
	}, {
		type: 'input',
		name: 'user_name',
		message: 'User Name:',
		validate: function(value){
			return value.length ? true : 'Please enter a valid user name';
		}
	}, {
		type: 'password',
		name: 'user_password',
		message: 'Password:',
		validate: function(value){
			return value.length ? true : 'Please enter a valid password.';
		}
	}];

	return inquirer.prompt(questions);
};

exports.askGitignoreQuestions = function(fileList){
	const questions = [{
		type: 'checkbox',
		name: 'ignore',
		choices: ['node_modules', ...fileList],
		message: 'Select the files and/or folders you wish to ignore:',
		default: ['node_modules', '.env']
	}];	

	return inquirer.prompt(questions);
};

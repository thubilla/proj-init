#! /usr/bin/env node

const fs = require('fs');
const argv = require('minimist')(process.argv.slice(2));
const shell = require('shelljs');
const chalk = require('chalk');

const { auth, run } = require('./lib/run');

if(argv.auth || argv.a){
	auth(process.cwd());
	return;
}

run(argv._[0], argv._[1]);
